package org.CelciusToFahrenheit;

public class Converter {

    public double convert(String celS){
        double cel = Double.valueOf(celS);
        return cel*(9.0/5.0)+32;
    }
}
